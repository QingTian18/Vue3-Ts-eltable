# qt-waterMark

是一个基于uni-app live-pusher的纯前端水印相机插件，目前相机主界面支持临时照片预览,闪光灯开关（闪光灯方法在iOS系统存在问题，目前暂时屏蔽iOS使用），美颜，美白开关，水印开关，自定义水印内容等操作。

## 使用

在使用过程有问题可以去[uniapp水印相机](https://juejin.cn/post/7391277159610433563)评论区@作者进行询问。
如果有比较有趣的小功能，也欢迎评论，作者会根据时间，尽量都增加。

直接下载插件到你的项目中，在需要的页面直接使用即可
```html
// 使用示例
<template>
	<view class="content">
		<qt-waterMark></qt-waterMark>
	</view>
</template>
```

***重要***
1.2.0版本为了实现拍照后的图片阅览功能，通过uni.getStorage将图片的临时地址进行了缓存，因此你可以通过读取缓存key:"imageUrls",来得到图片的地址（注意：得到的是一个数组）。
这里还要提醒您，因为缓存的时uniapp生成的临时地址，因此需要尽快将其上传到您的图片服务器，同时清空缓存文件，以免地址失效。


```js
// 获取到水印图片地址
uni.getStorageSync('imageUrls')
```

```json
// 地址示例
[
    {
        "url": "_doc/uniapp_temp_1741671498036/canvas/17416715192310.png",
        "listItem": [
            {
                "key": "时间：",
                "value": "2025-03-11 13:38:35",
                "status": true
            }
        ],
        "markShow": true,
        "isMark": true,
        "isSelect": true
    },
    {
        "url": "_doc/uniapp_temp_1741662113064/canvas/17416626690831.png",
        "listItem": [
            {
                "key": "时间：",
                "value": "2025-03-11 11:11:04",
                "status": true
            }
        ],
        "markShow": true,
        "isMark": true,
        "isSelect": true
    },
    {
        "url": "_doc/uniapp_temp_1741662113064/canvas/17416626431320.png",
        "listItem": [
            {
                "key": "哈哈哈",
                "value": "哈哈哈",
                "status": true
            },
            {
                "key": "时间：",
                "value": "2025-03-11 11:10:38",
                "status": true
            }
        ],
        "markShow": true,
        "isMark": true,
        "isSelect": true
    }
]
```


你也可以直接下载示例demo，查看水印相机插件的使用过程。