type Type = "txt" | "tag" | "range" | "date" | "img" | "select";

export interface TableColumns<U> {
	type: Type;
	prop: keyof U;
	label: string;
	width?: number;
	enum?: () => U[];
	fieldNames: {
		label: string;
		value: string;
	};
}

export interface TableConfigTest<T> {
	forEach(arg0: (item: any) => void): unknown;
	columns: TableColumns<T>[];
}
