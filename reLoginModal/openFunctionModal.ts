import { createApp, h } from "vue"; // 导入必要的函数

// 这是一个用来动态打开模态框的函数
function openModal(component: any, props: { [key: string]: any }) {
	// 创建一个div元素作为挂载点
	const container = document.createElement("div");
	document.body.appendChild(container);

	// 使用组件创建一个Vue应用实例
	const app = createApp({
		// 创建一个返回组件VNode的render函数
		render() {
			return h(component, {
				...props, // 展开任意附加的props
				// 定义一个关闭事件处理器
				onClose: () => {
					// 这将在组件发出'close'事件时被调用
					app.unmount(); // 清理工作，卸载应用
					document.body.removeChild(container); // 移除挂载点
				}
			});
		}
	});
	// 将Vue应用实例挂载到容器上
	app.mount(container);
}

// 最后，导出openModal函数
export { openModal };
