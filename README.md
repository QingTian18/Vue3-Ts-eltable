# QT组件库

### 介绍

1，Vue3+Ts 二次封装 el-table
实现 table 定制化，删除行，增加行

2，Echart 组件，包括柱状图、环形图、仪表盘组件

3，token失效时，在失效页面弹出登录框重新登录

4，uni-app 水印相机

### 更新日志

#### 2024/07/14
增加一个使用uniapp编写的水印相机，实现实时预览水印以及照片的水印添加

#### 2024/04/12 更新

增加一个方法，在 token 失效时，不跳转页面，在当前页面重新登录，以此解决页面填写内容以后，因为 token 失效导致的数据丢失，从而重新填写的问题（reLoginModal）

#### 10.8 更新

优化自定义列宽与滚动
优化范围输入

#### 9.4 更新

增加 date(单元格里日期选择),rang（范围输入），img（图片展示），select(下拉选择) 样式

增加项目中使用到的 Echart 组件，包括柱状图、环形图、仪表盘组件


### 使用演示

#### reLoginModal 使用演示

```TypeScript
import { openModal } from "url/reLoginModal/components/function/openFunctionModal";
import CLoginModal from "url/reLoginModal/components/CLoginModal.vue";
/**
		 * @description 响应拦截器
		 *  服务器换返回信息 -> [拦截统一处理] -> 客户端JS获取到信息
		 */
		this.service.interceptors.response.use(
			(response: AxiosResponse) => {
				const { data } = response;
				// * 在请求结束后，并关闭请求 loading
				tryHideFullScreenLoading();
				// * 登陆失效（code == 401）
				console.log("响应码：", data.code);
				if (data.code === ResultEnum.TOKEN_INVALID || data.code === ResultEnum.TOKEN_OVERDUE) {
					if (!isShowingTokenDialog) {
						isShowingTokenDialog = true;
						const globalStore = GlobalStore();
						ElMessageBox.confirm("token失效，请重新登录", "重新登录", {
							confirmButtonText: "在当前页重新登录",
							cancelButtonText: "取消",
							type: "warning",
							closeOnPressEscape: false,
							closeOnClickModal: false,
							showClose: false
						})
							.then(() => {
								console.log("执行了then");
								openModal(CLoginModal, {
									userAccount:
										globalStore.userCacheData &&
										globalStore.userCacheData.userAccount &&
										globalStore.userCacheData.userAccount.length
											? globalStore.userCacheData.userAccount
											: ""
								}); // globalStore.userCacheData.userAccount
								isShowingTokenDialog = false;
								// console.log(route.path);
								// if (route.path !== "/login") {
								// 	openModal(CLoginModal, { message: "去登录" });
								// }
							})
							.catch(() => {
								isShowingTokenDialog = false;
								globalStore.setToken("");
								router.replace(LOGIN_URL);
								// return Promise.reject(data);
							});
					}
					return false;
				}
				// * 成功请求（在页面上除非特殊情况，否则不用在页面处理失败逻辑）
				return data;
			},
		);
```

#### myTable 使用演示

```html
<MyTable :config="tableConfigEdu" :data="tableDataEdu" :border="true" />
<script setup lang="ts">
  interface TableDataTrain {
    startDate: string;
    endDate: string;
    address: string;
    content: string;
  }
  const tableConfigTrain = ref<TableConfigTest<TableDataTrain>>({
    columns: [
      {
        type: "txt",
        prop: "startDate",
        label: "开始日期",
        width: 200,
      },
      {
        type: "txt",
        prop: "endDate",
        label: "截止日期",
        width: 200,
      },
      {
        type: "txt",
        prop: "address",
        label: "地点",
      },
      {
        type: "txt",
        prop: "content",
        label: "内容",
      },
    ],
  });
  const tableDataTrain = ref<TableDataTrain[]>([
    { startDate: "", endDate: "", address: "", content: "" },
  ]);
</script>
```

### 声明

封装组件是为了更好的工作，随着工作的需求增加，会不断增加组件的功能，同时也会更新文档。
